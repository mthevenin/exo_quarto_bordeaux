# exo_quarto_bordeaux

Exercice pour la formation du 21 mars 2023

* Récupérer les fichiers suivants => ouvrir l'onglet Code => Download source file dans une archive: 
  * fecondite_ggp.csv
  * index.txt
  * programme.Récupérer

* Dans Gitlab créer un dépôt de type **website**

* Dans R créer un projet (version control)

* Ajouter les 3 fichiers de ce dépôt

* Editer en .qmd le fichier index.txt

* Si on a le temps déployer la page sur Gitlab: ajouter fichier `_quarto.yml` avec:

```yaml
project:
  type: website
  output_dir: public
```
